package com.example.qoltraining.user_flow.home;

import android.graphics.Color;

import com.example.qoltraining.bases.ArrayListClass;
import com.example.qoltraining.bases.ChartEntry;
import com.example.qoltraining.custom_views.MatrixChartView;
import com.example.qoltraining.data.models.MatrixChartViewModel;
import com.example.qoltraining.data.models.MatrixItem;
import com.example.qoltraining.data.models.MatrixStatusModel;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

import io.reactivex.rxjava3.core.Single;

public class HomeRepositry {



    HomeModel homeModel = new HomeModel();

    MatrixChartViewModel matrixChartViewModel;
    ArrayList<MatrixStatusModel> xAxismatrixStatusModelArrayList;
    ArrayList<MatrixStatusModel> yAxismatrixStatusModelArrayList;
    ArrayList<MatrixItem> matrixItems;
    ArrayList<MatrixItem> matrixItems2;
    ArrayList<MatrixItem> matrixItems3;
    ArrayList<MatrixItem> matrixItems4;
    ArrayList<MatrixItem> matrixItems5;




    public Single<HomeModel> getCharts(){

        setUpArrayLists();
        homeModel.setupArrayListClasses();
        return Single.fromCallable(() -> homeModel);
    }














    public void setUpArrayLists(){
        ArrayList<ChartEntry> arrayList = new ArrayList<>();
        ChartEntry entry1 = new ChartEntry(82,"تم التعاقد", "#44823D");
        ChartEntry entry2 = new ChartEntry(63,"لم يتم التعاقد","#A0CF73");
        arrayList.add(entry1);
        arrayList.add(entry2);
        homeModel.setArrayList(arrayList);

        ArrayList<ChartEntry> arrayList1 = new ArrayList<>();
        ChartEntry entry3 = new ChartEntry(68,"مركز أداء","#D43F3F");
        ChartEntry entry4 = new ChartEntry(26,"فريق العمل الدائم", "#F6B519");
        ChartEntry entry5 = new ChartEntry(18,"اللجنة الإستراتيجية","#F79B75");
        ChartEntry entry6 = new ChartEntry(13,"مركز تحقيق كفاءة الإنفاق","#F26667");
        arrayList1.add(entry3);
        arrayList1.add(entry4);
        arrayList1.add(entry5);
        arrayList1.add(entry6);
        homeModel.setArrayList1(arrayList1);


        ArrayList<ChartEntry>  arrayList2 = new ArrayList<>();
        ChartEntry entry9 = new ChartEntry(19,"ﻟﻢ يتم قياسها","#787C84");
        ChartEntry entry7 = new ChartEntry(12,"انخفاض مستوى","#EEC452");
        ChartEntry entry8 = new ChartEntry(8,"تم تحقيق المستهدف", "#A0CF73");
        ChartEntry entry10 = new ChartEntry(6,"انخفاض ملحوظ","#F26667");

        arrayList2.add(entry7);
        arrayList2.add(entry8);
        arrayList2.add(entry9);
        arrayList2.add(entry10);
        homeModel.setArrayList2(arrayList2);

        ArrayList<ChartEntry>  arrayList3 = new ArrayList<>();
        ChartEntry entry11 = new ChartEntry(68,"تم تحقيق المستهدف","#A0CF73");
        ChartEntry entry12 = new ChartEntry(26,"انخفاض المستوى", "#EEC452");
        ChartEntry entry13 = new ChartEntry(18,"انخفاض ملحوظ","#F26667");
        ChartEntry entry14 = new ChartEntry(13,"ﻟﻢ يتم قياسها","#787C84");

        arrayList3.add(entry11);
        arrayList3.add(entry12);
        arrayList3.add(entry13);
        arrayList3.add(entry14);
        homeModel.setArrayList3(arrayList3);

        ArrayList<ChartEntry>  horizantalArrayList = new ArrayList<>();
        horizantalArrayList.add(new ChartEntry(9,"متوسط","#FDC012"));
        horizantalArrayList.add(new ChartEntry(2,"مرتفع","#F26667"));
        horizantalArrayList.add(new ChartEntry(1,"منخفض","#A0CF73"));
        homeModel.setHorizantalArrayList(horizantalArrayList);

        ArrayList<ChartEntry> barChartArray = new ArrayList<>();
        barChartArray.add(new ChartEntry(9,"السقف","#D3D3D3"));
        barChartArray.add(new ChartEntry(2,"الميزانية الموافق عليها","#44823D"));
        barChartArray.add(new ChartEntry(1,"المنصرف حتى تاريخه","#A0CF73"));
        barChartArray.add(new ChartEntry(1,"العقود المبرمة حتى تاريخه","#757A81"));
        homeModel.setBarChartArray(barChartArray);


    }



    public MatrixChartViewModel setupMatrixChart(){
        xAxismatrixStatusModelArrayList = new ArrayList<>();
        xAxismatrixStatusModelArrayList.add(new MatrixStatusModel(1,"high","#558B2F"));
        xAxismatrixStatusModelArrayList.add(new MatrixStatusModel(2,"very high","#2E7D32"));
        xAxismatrixStatusModelArrayList.add(new MatrixStatusModel(3,"low","#388E3C"));
        xAxismatrixStatusModelArrayList.add(new MatrixStatusModel(4,"very low","#D84315"));
        xAxismatrixStatusModelArrayList.add(new MatrixStatusModel(5,"fails","#FFAB91"));

        yAxismatrixStatusModelArrayList = new ArrayList<>();
        yAxismatrixStatusModelArrayList.add(new MatrixStatusModel(6,"high","#558B2F"));
        yAxismatrixStatusModelArrayList.add(new MatrixStatusModel(7,"very high","#2E7D32"));
        yAxismatrixStatusModelArrayList.add(new MatrixStatusModel(8,"low","#388E3C"));
        yAxismatrixStatusModelArrayList.add(new MatrixStatusModel(9,"very low","#D84315"));
        yAxismatrixStatusModelArrayList.add(new MatrixStatusModel(10,"fails","#FFAB91"));

        matrixItems = new ArrayList<>();
        matrixItems.add(new MatrixItem(20,"#558B2F",17,15));
        matrixItems.add(new MatrixItem(30,"#388E3C",32,4));
        matrixItems.add(new MatrixItem(40,"#558B2F",13,14));
        matrixItems.add(new MatrixItem(40,"#558B2F",13,14));
        matrixItems.add(new MatrixItem(40,"#558B2F",13,14));


        matrixItems2 = new ArrayList<>();
        matrixItems2.add(new MatrixItem(50,"#558B2F",22,32));
        matrixItems2.add(new MatrixItem(60,"#388E3C",76,11));
        matrixItems2.add(new MatrixItem(70,"#558B2F",12,12));
        matrixItems2.add(new MatrixItem(70,"#558B2F",12,12));
        matrixItems2.add(new MatrixItem(70,"#558B2F",12,12));

        matrixItems3 = new ArrayList<>();
        matrixItems3.add(new MatrixItem(50,"#558B2F",22,32));
        matrixItems3.add(new MatrixItem(60,"#388E3C",76,11));
        matrixItems3.add(new MatrixItem(70,"#558B2F",12,12));
        matrixItems3.add(new MatrixItem(70,"#558B2F",12,12));
        matrixItems3.add(new MatrixItem(70,"#558B2F",12,12));

        matrixItems4 = new ArrayList<>();
        matrixItems4.add(new MatrixItem(50,"#558B2F",22,32));
        matrixItems4.add(new MatrixItem(60,"#388E3C",76,11));
        matrixItems4.add(new MatrixItem(70,"#558B2F",12,12));
        matrixItems4.add(new MatrixItem(70,"#558B2F",12,12));
        matrixItems4.add(new MatrixItem(70,"#558B2F",12,12));

        matrixItems5 = new ArrayList<>();
        matrixItems5.add(new MatrixItem(50,"#558B2F",22,32));
        matrixItems5.add(new MatrixItem(60,"#388E3C",76,11));
        matrixItems5.add(new MatrixItem(70,"#558B2F",12,12));
        matrixItems5.add(new MatrixItem(70,"#558B2F",12,12));
        matrixItems5.add(new MatrixItem(70,"#558B2F",12,12));

        ArrayList<ArrayList<MatrixItem>> arrayListArrayList = new ArrayList<>();
        arrayListArrayList.add(matrixItems);
        arrayListArrayList.add(matrixItems2);
        arrayListArrayList.add(matrixItems3);
        arrayListArrayList.add(matrixItems4);
        arrayListArrayList.add(matrixItems5);

        matrixChartViewModel = new MatrixChartViewModel("Risk","probanility","Impact",xAxismatrixStatusModelArrayList,yAxismatrixStatusModelArrayList,arrayListArrayList);
        return  matrixChartViewModel;



    }



}
