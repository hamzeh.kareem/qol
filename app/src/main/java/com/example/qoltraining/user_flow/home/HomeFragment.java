package com.example.qoltraining.user_flow.home;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.qoltraining.R;
import com.example.qoltraining.bases.ChartEntry;
import com.example.qoltraining.bases.LegendRecyclerViewAdapter;
import com.example.qoltraining.databinding.FragmentHomeBinding;
import com.example.qoltraining.utils.Event;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
FragmentHomeBinding binding;
HomeViewModel model;


LegendRecyclerViewAdapter acceptedAdapter;
LegendRecyclerViewAdapter nonAcceptedAdapter;
LegendRecyclerViewAdapter qualityActionsAdapter;
LegendRecyclerViewAdapter mainPerformanceAdapter;
LegendRecyclerViewAdapter riskManagementAdapter;
LegendRecyclerViewAdapter budgetAdapter;



    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_toolbar,menu);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment

        setHasOptionsMenu(true);
        model = new ViewModelProvider(this).get(HomeViewModel.class);
        binding = FragmentHomeBinding.inflate(getLayoutInflater());
        model.setMatrixChartViewMutableLiveData();
        binding.setModel(model);
        model.getData();
        model.getHomeModelMutableLiveData().observe(getViewLifecycleOwner(), new Observer<HomeModel>() {
            @Override
            public void onChanged(HomeModel homeModel) {
                setPieChart(homeModel.getArrayListClass().getPieEntries(),homeModel.getArrayListClass().getFirstPieColors(),null,binding.acceptedActionPieChart,homeModel.getArrayList());
                Log.i("entries",model.homeModelMutableLiveData.getValue().getArrayListClass().getPieEntries().toString());
                setPieChart(model.homeModelMutableLiveData.getValue().getArrayListClass1().getPieEntries(),model.homeModelMutableLiveData.getValue().getArrayListClass1().getFirstPieColors(),null,binding.nonAcceptedActionPieChart,model.homeModelMutableLiveData.getValue().getArrayList1());
                setPieChart(model.homeModelMutableLiveData.getValue().getArrayListClass2().getPieEntries(),model.homeModelMutableLiveData.getValue().getArrayListClass2().getFirstPieColors(),null,binding.qualityActionPieChart,model.homeModelMutableLiveData.getValue().getArrayList2());
                setPieChart(model.homeModelMutableLiveData.getValue().getArrayListClass3().getPieEntries(),model.homeModelMutableLiveData.getValue().getArrayListClass3().getFirstPieColors(),null,binding.mainPerformancePieChart,model.homeModelMutableLiveData.getValue().getArrayList3());
                setHorizantalBarChart(model.homeModelMutableLiveData.getValue().getHorizantalArrayListClass().getHorizantalChartEntries(),model.homeModelMutableLiveData.getValue().getHorizantalArrayListClass().getFirstPieColors(),binding.horizantalBarChart);
                setVerticalBarChart(model.homeModelMutableLiveData.getValue().getVerticalArrayListClass().getHorizantalChartEntries(),model.homeModelMutableLiveData.getValue().getVerticalArrayListClass().getFirstPieColors(),binding.BarChart);


            }
        });

        model.getShowLoading().observe(getViewLifecycleOwner(), new Observer<Event<Object>>() {
            @Override
            public void onChanged(Event<Object> objectEvent) {
                Toast.makeText(getContext(), "Loading", Toast.LENGTH_SHORT).show();
            }
        });
        model.getShowHideLoading().observe(getViewLifecycleOwner(), new Observer<Event<Object>>() {
            @Override
            public void onChanged(Event<Object> objectEvent) {
                Toast.makeText(getContext(), "done", Toast.LENGTH_SHORT).show();
            }
        });
//        setupAdapters();


        return binding.getRoot();

    }

    @Override
    public void onResume() {
        super.onResume();











    }

    public void setupAdapters(){
        acceptedAdapter = new LegendRecyclerViewAdapter(getContext(),model.homeModelMutableLiveData.getValue().getArrayList());
        nonAcceptedAdapter = new LegendRecyclerViewAdapter(getContext(),model.homeModelMutableLiveData.getValue().getArrayList1());
        qualityActionsAdapter = new LegendRecyclerViewAdapter(getContext(),model.homeModelMutableLiveData.getValue().getArrayList2());
        mainPerformanceAdapter = new LegendRecyclerViewAdapter(getContext(),model.homeModelMutableLiveData.getValue().getArrayList3());
        riskManagementAdapter = new LegendRecyclerViewAdapter(getContext(),model.homeModelMutableLiveData.getValue().getHorizantalArrayList());
        budgetAdapter = new LegendRecyclerViewAdapter(getContext(),model.homeModelMutableLiveData.getValue().getBarChartArray());

        callAdapters(acceptedAdapter,binding.acceptedActionsRecyclerView);
        callAdapters(nonAcceptedAdapter,binding.nonAcceptespieRecyclerView);
        callAdapters(qualityActionsAdapter,binding.qualityActionspieRecyclerView);
        callAdapters(mainPerformanceAdapter,binding.mainPerformanceRecyclerView);
        callAdapters(riskManagementAdapter,binding.riskeRecyclerView);
        callAdapters(budgetAdapter,binding.budgetRecyclerView);
    }

    public View callAdapters(LegendRecyclerViewAdapter adapter, RecyclerView view){
        RecyclerView recyclerView = view;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        recyclerView.setAdapter(adapter);

        return recyclerView;
    }
    public void setHorizantalBarChart(ArrayList<BarEntry> list, int[] colors, BarChart chart){
        BarDataSet dataSet = new BarDataSet(list,null);
        dataSet.setColors(colors);
        BarData data = new BarData(dataSet);
        data.setBarWidth(4.5f);
//        adapter.addItems(arrayList);
        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(false);
        YAxis yAxis = chart.getAxisRight();
        yAxis.setEnabled(false);
        YAxis yAxis1 = chart.getAxisLeft();
        yAxis1.setEnabled(false);
        chart.getDescription().setEnabled(false);
        chart.getLegend().setEnabled(false);

        chart.setData(data);
    }

    public void setVerticalBarChart(ArrayList<BarEntry> arrayList, int[] colors, BarChart chart  ){
        BarDataSet dataSet = new BarDataSet(arrayList,null);
        dataSet.setColors(colors);
        BarData barData = new BarData();
        barData.addDataSet(dataSet);
        XAxis xAxis = chart.getXAxis();
//        xAxis.setEnabled(false);
        YAxis yAxis = chart.getAxisRight();
        yAxis.setEnabled(false);
        YAxis yAxis1 = chart.getAxisLeft();
        int maxCapacity = 100;
        LimitLine ll = new LimitLine(maxCapacity, "Max Capacity");
        chart.getAxisLeft().addLimitLine(ll);
//        yAxis1.setEnabled(false);
        chart.getDescription().setEnabled(false);
        chart.getLegend().setEnabled(false);
        chart.setData(barData);
        chart.invalidate();
    }

    public void setPieChart(ArrayList<PieEntry> pieEntries, int[] colors, String centreText, PieChart chart, ArrayList<ChartEntry> chartEntries){
        PieDataSet dataSet = new PieDataSet(pieEntries,null);
        dataSet.setColors(colors);
        dataSet.setValueTextColor(Color.BLACK);
        dataSet.setValueTextSize(17);
        dataSet.setDrawValues(false);
        PieData pieData = new PieData(dataSet);
        chart.setData(pieData);
        chart.setDrawEntryLabels(false);
        chart.getDescription().setEnabled(false);
        chart.getLegend().setEnabled(false);
        chart.setCenterText(centreText);
        chart.setCenterTextSize(35);
        chart.animate();
    }



}

