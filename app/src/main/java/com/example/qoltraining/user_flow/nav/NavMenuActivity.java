package com.example.qoltraining.user_flow.nav;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;

import com.example.qoltraining.R;
import com.example.qoltraining.databinding.ActivityNavMenuBinding;
import com.example.qoltraining.user_flow.home.HomeFragment;

public class NavMenuActivity extends AppCompatActivity {
    NavMenuViewModel model;
    ActivityNavMenuBinding binding;
    int selectedItemId;
    protected HomeFragment homeFragment;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fragment_toolbar, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNavMenuBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        homeFragment = new HomeFragment();

        binding.setModel(model);

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {


            binding.bottomNavigation.setOnNavigationItemSelectedListener(item -> {
                switch (item.getItemId()) {
                    case R.id.page_1:
                        showFragment(R.id.content, homeFragment, false);

                        break;

                    case R.id.page_2:
                        selectedItemId = R.id.page_2;
                        Toast.makeText(this, "page2", Toast.LENGTH_SHORT).show();

                        break;

                    case R.id.page_3:
                        selectedItemId = R.id.page_3;
                        Toast.makeText(this, "page3", Toast.LENGTH_SHORT).show();

                        break;


                    case R.id.page_4:
                        selectedItemId = R.id.page_4;
                        Toast.makeText(this, "page4", Toast.LENGTH_SHORT).show();

                        break;
                }
                return true;

            });
            binding.bottomNavigation.setSelectedItemId(R.id.page_1);
        } else {
            showFragment(R.id.content, homeFragment, false);
        }


    }

    public void showFragment(@IdRes int container, Fragment fragment, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction()
                .replace(container, fragment);

        if (addToBackStack) {
            ft.addToBackStack(null);
        }

        ft.commit();

    }

    public static void launchActivity(Context context) {
        Intent intent = new Intent(context, NavMenuActivity.class);
        context.startActivity(intent);
    }
}

