package com.example.qoltraining.user_flow.user_managment;

import android.content.Context;

import com.example.qoltraining.network.JsonHolder;
import com.example.qoltraining.network.RetrofitClient;
import com.example.qoltraining.user_flow.user_managment.login.model.LoginModel;
import com.example.qoltraining.user_flow.user_managment.login.model.LoginRequestModel;
import com.google.gson.annotations.SerializedName;

import io.reactivex.rxjava3.core.Single;
import retrofit2.Retrofit;

public class UserManagmentRepositry {
    RetrofitClient retrofitClient = new RetrofitClient();
    Context context;
    private JsonHolder jsonHolder;

    public UserManagmentRepositry(Context context) {
        this.context = context;
        jsonHolder = retrofitClient.getRetrofitWithOkhttp(context).create(JsonHolder.class);
    }

    UserInfoRepositry userInfoRepositry = new UserInfoRepositry();
    public Single<LoginModel> getLoginObservable(String userName, String password){
        return jsonHolder.login(new LoginRequestModel(userName,password));

    }

    public void setIsLoggedIn(boolean loggedIn){
        userInfoRepositry.setLoggedIn(loggedIn);
    }

}
