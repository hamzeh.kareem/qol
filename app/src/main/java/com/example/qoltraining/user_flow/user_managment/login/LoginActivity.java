package com.example.qoltraining.user_flow.user_managment.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;


import com.example.qoltraining.databinding.ActivityLoginBinding;
import com.example.qoltraining.dialogs.LoadingDialog;
import com.example.qoltraining.user_flow.nav.NavMenuActivity;
import com.example.qoltraining.utils.Event;
import com.example.qoltraining.utils.LocaleHelper;

public class LoginActivity extends AppCompatActivity {
    ActivityLoginBinding binding;
    LoginViewModel model;
    private LoadingDialog loadingDialog;
    private static final String LOADING_DIALOG_TAG = "loading_dialog";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        model = new ViewModelProvider(this).get(LoginViewModel.class);
        binding.setViewModel(model);

        //Observing starts here
        model.getOpenNavActivity().observe(this, aVoid ->
                {
                    if (aVoid.getContentIfNotHandled() != null)
                        enterTheHomeScreen();
                }
        );

        model.getShowLoginError().observe(this,aVoid -> {
            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
        });
        model.getShowLoading().observe(this,aVoid -> {

            showDialog();
        });
        model.getShowHideLoading().observe(this,aVoid -> {

            hideDialog();
        });

        model.getEmptyUsernameOrPassword().observe(this,aVoid -> {
            Toast.makeText(this, "empty username or password", Toast.LENGTH_SHORT).show();
        });


    }

    public void enterTheHomeScreen(){
        NavMenuActivity.launchActivity(this);
    }

    public void showDialog(){
        loadingDialog = LoadingDialog.newInstance("Loading");

        loadingDialog.show(getSupportFragmentManager(), LOADING_DIALOG_TAG);
    }

    public void hideDialog(){
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }
    


}