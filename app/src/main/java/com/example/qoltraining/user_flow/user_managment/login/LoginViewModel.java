package com.example.qoltraining.user_flow.user_managment.login;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.qoltraining.dialogs.LoadingDialog;
import com.example.qoltraining.user_flow.user_managment.UserManagmentRepositry;
import com.example.qoltraining.user_flow.user_managment.login.model.LoginModel;
import com.example.qoltraining.utils.Event;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class LoginViewModel extends AndroidViewModel {




    MutableLiveData<String> userName = new MutableLiveData<>();
    MutableLiveData<String> password = new MutableLiveData<>();
    MutableLiveData<Event<Object>> openNavActivity = new MutableLiveData<>();
    MutableLiveData<Event<Throwable>> showLoginError = new MutableLiveData<>();
    MutableLiveData<Event<Object>> showLoading = new MutableLiveData<>();
    MutableLiveData<Event<Object>> showHideLoading = new MutableLiveData<>();

    public MutableLiveData<Event<Object>> getEmptyUsernameOrPassword() {
        return emptyUsernameOrPassword;
    }

    MutableLiveData<Event<Object>> emptyUsernameOrPassword = new MutableLiveData<>();

    UserManagmentRepositry userManagmentRepositry = new UserManagmentRepositry(getApplication());
    CompositeDisposable compositeDisposable = new CompositeDisposable();



    public MutableLiveData<Event<Object>> getShowLoading() {
        return showLoading;
    }

    public LoginViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<String> getUserName() {
        return userName;
    }

    public MutableLiveData<String> getPassword() {
        return password;
    }

    public MutableLiveData<Event<Throwable>> getShowLoginError() {
        return showLoginError;
    }

    public void signInClicked(){
        if (isInputValid()){
            Single<LoginModel> loginObserVable = userManagmentRepositry.getLoginObservable(userName.getValue(),password.getValue());
            login(loginObserVable);
        }


    }

    public MutableLiveData<Event<Object>> getOpenNavActivity() {
        return openNavActivity;
    }

    public boolean isInputValid(){
        if (userName.getValue() != null && password .getValue() != null){
            return true;
        }

        else {
            emptyUsernameOrPassword.setValue(new Event<>(new Object()));
            return false;
        }
    }

    public void login(Single<LoginModel> loginObservable){
        Disposable loginDisposable = loginObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    showLoading();
                })
                .doOnEvent((loginModel, throwable) -> hideLoading())
                .subscribe((LoginModel loginModel) -> {

                    if (loginModel.getSuccess()) {
                        userManagmentRepositry.setIsLoggedIn(true);
                        openNavActivity.setValue(new Event<>(new Object()));
                    }
                    else {

                    }

                }, this::handleError);

        compositeDisposable.add(loginDisposable);
    }

    public void handleError(Throwable throwable){
        showLoginError.setValue(new Event<>(throwable));
    }
    public void showLoading(){
        showLoading.setValue(new Event<>(new Object()));
    }

    public MutableLiveData<Event<Object>> getShowHideLoading() {
        return showHideLoading;
    }

    public void hideLoading(){
        showHideLoading.setValue(new Event<>(new Object()));

    }
}
