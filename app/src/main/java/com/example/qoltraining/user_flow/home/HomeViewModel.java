package com.example.qoltraining.user_flow.home;

import android.content.Context;
import android.graphics.Color;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.qoltraining.bases.ChartEntry;
import com.example.qoltraining.custom_views.MatrixChartView;
import com.example.qoltraining.data.models.MatrixChartViewModel;
import com.example.qoltraining.data.models.MatrixItem;
import com.example.qoltraining.data.models.MatrixStatusModel;
import com.example.qoltraining.utils.Event;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class HomeViewModel extends ViewModel {

    MutableLiveData<MatrixChartViewModel> matrixChartViewMutableLiveData = new MutableLiveData<>();
    MutableLiveData<HomeModel> homeModelMutableLiveData = new MutableLiveData<>();
    MutableLiveData<Event<Object>> showLoading = new MutableLiveData<>();
    MutableLiveData<Event<Object>> showHideLoading = new MutableLiveData<>();

    public MutableLiveData<Event<Object>> getShowLoading() {
        return showLoading;
    }

    public void setShowLoading(MutableLiveData<Event<Object>> showLoading) {
        this.showLoading = showLoading;
    }

    public MutableLiveData<Event<Object>> getShowHideLoading() {
        return showHideLoading;
    }

    public void setShowHideLoading(MutableLiveData<Event<Object>> showHideLoading) {
        this.showHideLoading = showHideLoading;
    }

    HomeRepositry homeRepositry = new HomeRepositry();

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    public MutableLiveData<HomeModel> getHomeModelMutableLiveData() {
        return homeModelMutableLiveData;
    }


    public MutableLiveData<MatrixChartViewModel> getMatrixChartViewMutableLiveData() {
        return matrixChartViewMutableLiveData;
    }

    public void setMatrixChartViewMutableLiveData() {
        this.matrixChartViewMutableLiveData.setValue(homeRepositry.setupMatrixChart());
    }

    public void getData() {
        compositeDisposable.add(homeRepositry.getCharts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent((loginModel, throwable) -> hideLoading())
                .doOnSubscribe((Disposable disposable) -> {
                    showLoading();
                })
                .subscribe((HomeModel homeModel) -> {
                    homeModelMutableLiveData.setValue(homeModel);
                }, this::handleError));
    }

    private void showLoading() {
        showLoading.setValue(new Event<>(new Object()));
    }

    private void hideLoading() {
        showHideLoading.setValue(new Event<>(new Object()));
    }

    public void handleError(Throwable throwable){
        Context context = null;
        Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
    }

//    public void setHomeModelCharts(PieChart accepted, PieChart nonAccepted, PieChart performance, PieChart mainPerformance, HorizontalBarChart riskManagementBarChart, BarChart vertical,MatrixChartView matrixChartView){
//        homeRepositry.homeModel.setAcceptedActions(accepted);
//        homeRepositry.homeModel.setNonAcceptedActions(nonAccepted);
//        homeRepositry.homeModel.setMainPerformanceActions(mainPerformance);
//        homeRepositry.homeModel.setPerformanceActions(performance);
//        homeRepositry.homeModel.setRiskManagmentBarChart(riskManagementBarChart);
//        homeRepositry.homeModel.setBudgetBarChart(vertical);
//        homeRepositry.homeModel.setRiskMatrixChart(matrixChartView);
//    }



}
