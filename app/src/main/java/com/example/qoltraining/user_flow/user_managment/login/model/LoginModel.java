package com.example.qoltraining.user_flow.user_managment.login.model;

import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("resuly")
    private LoginResultClass result;
    @SerializedName("success")
    private boolean success;
    @SerializedName("error")
    private String error;
    @SerializedName("httpStatusCode")
    int httpStatusCode;

    public LoginResultClass getResult() {
        return result;
    }

    public boolean getSuccess() {
        return success;
    }

    public String getError() {
        return error;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }
}
