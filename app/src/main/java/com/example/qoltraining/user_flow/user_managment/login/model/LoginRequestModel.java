package com.example.qoltraining.user_flow.user_managment.login.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class LoginRequestModel implements Parcelable {
    @SerializedName("userName")
    private String userName;
    @SerializedName("password")
    private String password;

    public LoginRequestModel(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    protected LoginRequestModel(Parcel in) {
        userName = in.readString();
        password = in.readString();
    }

    public static final Creator<LoginRequestModel> CREATOR = new Creator<LoginRequestModel>() {
        @Override
        public LoginRequestModel createFromParcel(Parcel in) {
            return new LoginRequestModel(in);
        }

        @Override
        public LoginRequestModel[] newArray(int size) {
            return new LoginRequestModel[size];
        }
    };

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(password);
    }
}
