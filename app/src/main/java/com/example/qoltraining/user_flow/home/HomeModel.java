package com.example.qoltraining.user_flow.home;

import com.example.qoltraining.bases.ArrayListClass;
import com.example.qoltraining.bases.ChartEntry;
import com.example.qoltraining.custom_views.MatrixChartView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;

import java.util.ArrayList;

public class HomeModel {
    ArrayListClass arrayListClass;
    ArrayListClass arrayListClass1;
    ArrayListClass arrayListClass2;
    ArrayListClass arrayListClass3;
    ArrayListClass horizantalArrayListClass;
    ArrayListClass verticalArrayListClass;


    ArrayList<ChartEntry> arrayList;
    ArrayList<ChartEntry> arrayList1;
    ArrayList<ChartEntry> arrayList2;
    ArrayList<ChartEntry> arrayList3;
    ArrayList<ChartEntry> horizantalArrayList;
    ArrayList<ChartEntry> barChartArray;

    public HomeModel(){

    }

    public ArrayListClass getArrayListClass() {
        return arrayListClass;
    }

    public void setArrayListClass(ArrayListClass arrayListClass) {
        this.arrayListClass = arrayListClass;
    }

    public ArrayListClass getArrayListClass1() {
        return arrayListClass1;
    }

    public void setArrayListClass1(ArrayListClass arrayListClass1) {
        this.arrayListClass1 = arrayListClass1;
    }

    public ArrayListClass getArrayListClass2() {
        return arrayListClass2;
    }

    public void setArrayListClass2(ArrayListClass arrayListClass2) {
        this.arrayListClass2 = arrayListClass2;
    }

    public ArrayListClass getArrayListClass3() {
        return arrayListClass3;
    }

    public void setArrayListClass3(ArrayListClass arrayListClass3) {
        this.arrayListClass3 = arrayListClass3;
    }

    public ArrayListClass getHorizantalArrayListClass() {
        return horizantalArrayListClass;
    }

    public void setHorizantalArrayListClass(ArrayListClass horizantalArrayListClass) {
        this.horizantalArrayListClass = horizantalArrayListClass;
    }

    public ArrayListClass getVerticalArrayListClass() {
        return verticalArrayListClass;
    }

    public void setVerticalArrayListClass(ArrayListClass verticalArrayListClass) {
        this.verticalArrayListClass = verticalArrayListClass;
    }

    public ArrayList<ChartEntry> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<ChartEntry> arrayList) {
        this.arrayList = arrayList;
    }

    public ArrayList<ChartEntry> getArrayList1() {
        return arrayList1;
    }

    public void setArrayList1(ArrayList<ChartEntry> arrayList1) {
        this.arrayList1 = arrayList1;
    }

    public ArrayList<ChartEntry> getArrayList2() {
        return arrayList2;
    }

    public void setArrayList2(ArrayList<ChartEntry> arrayList2) {
        this.arrayList2 = arrayList2;
    }

    public ArrayList<ChartEntry> getArrayList3() {
        return arrayList3;
    }

    public void setArrayList3(ArrayList<ChartEntry> arrayList3) {
        this.arrayList3 = arrayList3;
    }

    public ArrayList<ChartEntry> getHorizantalArrayList() {
        return horizantalArrayList;
    }

    public void setHorizantalArrayList(ArrayList<ChartEntry> horizantalArrayList) {
        this.horizantalArrayList = horizantalArrayList;
    }

    public ArrayList<ChartEntry> getBarChartArray() {
        return barChartArray;
    }

    public void setBarChartArray(ArrayList<ChartEntry> barChartArray) {
        this.barChartArray = barChartArray;
    }

    public void setupArrayListClasses(){
        this.arrayListClass = new ArrayListClass(arrayList);
        this.arrayListClass1 = new ArrayListClass(arrayList1);
        this.arrayListClass2 = new ArrayListClass(arrayList2);
        this.arrayListClass3 = new ArrayListClass(arrayList3);
        this.horizantalArrayListClass = new ArrayListClass(horizantalArrayList);
        this.verticalArrayListClass = new ArrayListClass(barChartArray);
    }





}
