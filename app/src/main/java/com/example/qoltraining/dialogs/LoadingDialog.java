package com.example.qoltraining.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.example.qoltraining.R;

public class LoadingDialog extends DialogFragment {

    public static final String TITLE_ARGS = "title";

    public static LoadingDialog newInstance(String title) {

        Bundle args = new Bundle();

        LoadingDialog fragment = new LoadingDialog();
        args.putString(TITLE_ARGS, title);
        fragment.setArguments(args);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.progress_dialog, null);
        TextView progressDialogText = view.findViewById(R.id.tv_progress_text);
        if (getArguments() != null)
            progressDialogText.setText(getArguments().getString(TITLE_ARGS));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.TransparentProgressAlertDialog);

        builder.setView(view);
        builder.setCancelable(false);


        return builder.create();
    }
}
