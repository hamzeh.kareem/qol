package com.example.qoltraining.custom_views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.example.qoltraining.R;
import com.example.qoltraining.bases.ColorsHelper;
import com.example.qoltraining.data.models.MatrixChartViewModel;
import com.example.qoltraining.data.models.MatrixStatusModel;

import java.util.Locale;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class MatrixChartView extends LinearLayout {
    private MatrixChartViewModel matrixChartViewModel;

    private TableLayout tableLayout;

    private TextView tvXAxisLabel;
    private VerticalTextView tvYAxisLabel;

    private MatrixChartClickListener matrixChartClickListener;




    public MatrixChartView(Context context) {
        super(context);
        initializeView(context);
    }

    public MatrixChartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initializeView(context);
    }

    public MatrixChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeView(context);
    }

    public MatrixChartView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initializeView(context);
    }

    private void initializeView(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_matrix_chart, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        tableLayout = findViewById(R.id.table_layout);
        tvXAxisLabel = findViewById(R.id.tv_x_axis_title);
        tvYAxisLabel = findViewById(R.id.tv_y_axis_title);
    }

    public void setMatrixChartViewModel(MatrixChartViewModel matrixChartViewModel) {
        this.matrixChartViewModel = matrixChartViewModel;
        tvXAxisLabel.setText(matrixChartViewModel.getXAxisLabel());
        tvYAxisLabel.setText(matrixChartViewModel.getYAxisLabel());
        drawChart();
    }

    private void drawChart() {

        if (matrixChartViewModel != null) {

            Log.d("MATRIX", matrixChartViewModel.toString());

            if (matrixChartViewModel.getMatrix() != null && matrixChartViewModel.getMatrix().size() > 0) {
                Log.d(getClass().getSimpleName(), "start drawing matrix chart");

                tableLayout.removeAllViews();

                int columns = matrixChartViewModel.getYAxisLegend().size();
                int rows = matrixChartViewModel.getXAxisLegend().size();

                ViewGroup.LayoutParams rowParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                TableRow.LayoutParams cellParams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1f);


                for (int i = 0; i < rows; i++) {
                    TableRow tableRow = new TableRow(getContext());
                    tableRow.setLayoutParams(rowParams);
                    for (int j = 0; j < columns + 1; j++) {
                        if (j == 0) {
                            //on the first item draw the legend item
                            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                            View cellView = inflater.inflate(R.layout.item_matrix_legand, null);
                            cellView.setLayoutParams(cellParams);

                            TextView tvStatusName = cellView.findViewById(R.id.tv_status_name);
                            tvStatusName.setText(matrixChartViewModel.getYAxisLegend().get(i).getStatusName());

                            View vStatusColor = cellView.findViewById(R.id.v_legend_status_color);

                            vStatusColor.setBackground(
                                    ColorsHelper.getRoundedCornerDrawable(
                                            Color.parseColor(matrixChartViewModel.getYAxisLegend().get(i).getStatusColor()),
                                            6));

                            int finalLegendCellIndex = j;
                            cellView.setOnClickListener(view -> {
                                if (matrixChartClickListener != null)
                                    matrixChartClickListener.onYLegendClicked(matrixChartViewModel.getYAxisLegend().get(finalLegendCellIndex));
                            });

                            cellView.setBackground(ColorsHelper.getRippleDrawable(
                                    getResources().getColor(R.color.white),
                                    Color.parseColor(matrixChartViewModel.getYAxisLegend().get(i).getStatusColor()),
                                    12,
                                    12));

                            tableRow.addView(cellView);
                        } else {
                            //on other rows draw a button
                            TextView textView = new TextView(getContext());
                            textView.setLayoutParams(cellParams);
                            textView.setClickable(true);
                            textView.setFocusable(true);
                            textView.setBackground(
                                    ColorsHelper.getRippleDrawable(
                                            Color.parseColor(matrixChartViewModel.getMatrix().get(i).get(j - 1).getItemColor()),
                                            getResources().getColor(R.color.white),
                                            12,
                                            12));
                            textView.setText(String.format(Locale.US, "%d", matrixChartViewModel.getMatrix().get(i).get(j - 1).getCount()));
                            textView.setGravity(Gravity.CENTER);
                            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                            textView.setTextColor(getResources().getColor(R.color.white));
//                            textView.setTypeface(ResourcesCompat.getFont(getContext(), R.font.bahij_the_sans_arabic_bold));

                            int finalI = i;
                            int finalJ = j;
                            textView.setOnClickListener(v ->
                                    matrixChartClickListener.onMatrixItemClicked(
                                            matrixChartViewModel.getMatrix().get(finalI).get(finalJ - 1).getImpactStatusCode(),
                                            matrixChartViewModel.getMatrix().get(finalI).get(finalJ - 1).getProbabilityStatusCode()));

                            tableRow.addView(textView);
                        }
                    }
                    tableLayout.addView(tableRow);
                }

                //add the last row that has all the legends
                TableRow lastTableRow = new TableRow(getContext());
                lastTableRow.setLayoutParams(rowParams);

                //the first view is an empty view to add space
                View emptyView = new View(getContext());
                emptyView.setLayoutParams(cellParams);
                lastTableRow.addView(emptyView);

                for (int i = 0; i < columns; i++) {
                    //on the first item draw the legend item
                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                    View cellView = inflater.inflate(R.layout.item_matrix_legand, null);
                    cellView.setLayoutParams(cellParams);

                    TextView tvStatusName = cellView.findViewById(R.id.tv_status_name);
                    tvStatusName.setText(matrixChartViewModel.getXAxisLegend().get(i).getStatusName());

                    View vStatusColor = cellView.findViewById(R.id.v_legend_status_color);

                    vStatusColor.setBackground(
                            ColorsHelper.getRoundedCornerDrawable(
                                    Color.parseColor(matrixChartViewModel.getXAxisLegend().get(i).getStatusColor()),
                                    6)
                    );

                    int finalLegendCellIndex = i;
                    cellView.setOnClickListener(view -> {
                        if (matrixChartClickListener != null)
                            matrixChartClickListener.onXLegendClicked(matrixChartViewModel.getXAxisLegend().get(finalLegendCellIndex));
                    });

                    cellView.setBackground(
                            ColorsHelper.getRippleDrawable(
                                    getResources().getColor(R.color.white),
                                    Color.parseColor(matrixChartViewModel.getXAxisLegend().get(i).getStatusColor()),
                                    12,
                                    12));

                    lastTableRow.addView(cellView);
                }
                tableLayout.addView(lastTableRow);
                Log.d(getClass().getSimpleName(), "finish drawing matrix chart");
            } else {
                Log.d(getClass().getSimpleName(), "matrix is null or empty");
            }
        } else {
            Log.d(getClass().getSimpleName(), "chart model is null");
        }
    }

    public interface MatrixChartClickListener {
        void onXLegendClicked(MatrixStatusModel matrixStatusModel);

        void onYLegendClicked(MatrixStatusModel matrixStatusModel);

        void onMatrixItemClicked(int impact, int probability);
    }
}
