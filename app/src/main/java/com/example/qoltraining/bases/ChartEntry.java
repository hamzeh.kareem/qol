package com.example.qoltraining.bases;


public class ChartEntry {
    private int value;
    private String label;
    private String color;

    public ChartEntry(int value, String label, String color) {
        this.value = value;
        this.label = label;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public String getLabel() {
        return label;
    }

    public String getColor() {
        return color;
    }
}
