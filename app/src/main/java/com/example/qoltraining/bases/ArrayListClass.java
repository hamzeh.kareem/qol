package com.example.qoltraining.bases;

import android.graphics.Color;

import androidx.lifecycle.MutableLiveData;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

public class ArrayListClass {
    ArrayList<ChartEntry> chartEntries;
    public ArrayListClass(ArrayList<ChartEntry>chartEntries) {
        this.chartEntries = chartEntries;
    }


    public ArrayList<PieEntry> getPieEntries(){

        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        for (ChartEntry entry : chartEntries){
            pieEntries.add(new PieEntry(entry.getValue(),entry.getLabel()));
        }
        return pieEntries;
    }




    public int [] getFirstPieColors(){
        int [] a = new int[chartEntries.size()];
        for (int i = 0 ; i<a.length ; i++){
            a[i] = Color.parseColor(chartEntries.get(i).getColor());
        }
        return a;
    }

    public ArrayList<BarEntry> getHorizantalChartEntries(){
        float i = 10;
        float j = 1;
        ArrayList<BarEntry> pieEntries = new ArrayList<>();

        for (ChartEntry entry : chartEntries){
            pieEntries.add(new BarEntry(i*j,entry.getValue()));
            j +=1;
        }
        return pieEntries;

    }

}
