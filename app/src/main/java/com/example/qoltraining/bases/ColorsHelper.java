package com.example.qoltraining.bases;

import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;

import androidx.annotation.ColorInt;
import androidx.core.graphics.ColorUtils;

public class ColorsHelper {
    public static boolean isColorDark(int color) {
        return ColorUtils.calculateLuminance(color) < 0.5;
    }

    public static ColorStateList getPressedColorSelector(int normalColor, int pressedColor) {
        return new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{android.R.attr.state_focused},
                        new int[]{android.R.attr.state_activated},
                        new int[]{}
                },
                new int[]{
                        pressedColor,
                        pressedColor,
                        pressedColor,
                        normalColor
                }
        );
    }

    public static RippleDrawable getRippleDrawable(@ColorInt int normalColor, @ColorInt int pressedColor, int cornerRadius, int inset) {
        //we use the gradient drawable to create a background with rounded corners and the normal color
        GradientDrawable normalBackgroundDrawable = ColorsHelper.getRoundedCornerDrawable(normalColor, cornerRadius);

        //then we wrap the gradient drawable with an inset drawable to add inset to the drawable (inset is basically like padding)
        InsetDrawable pressedBackgroundInsetWrapper = new InsetDrawable(normalBackgroundDrawable, inset, inset, inset, inset);

        //then we create the ripple drawable by creating a color selector, and using the inset drawable, and it will create the ripple effect
        return new RippleDrawable(ColorsHelper.getPressedColorSelector(normalColor, pressedColor), pressedBackgroundInsetWrapper, null);
    }

    public static GradientDrawable getRoundedCornerDrawable(@ColorInt int color, int alpha, int cornerRadius) {
        return getRoundedCornerDrawable(ColorUtils.setAlphaComponent(color, alpha), cornerRadius);
    }

    public static GradientDrawable getRoundedCornerDrawable(@ColorInt int color, int cornerRadius) {
        //we use the gradient drawable to create a background with rounded corners
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(color);
        gradientDrawable.setCornerRadius(cornerRadius);

        return gradientDrawable;
    }
}
