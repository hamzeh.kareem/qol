package com.example.qoltraining.bases;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.qoltraining.R;

import java.util.ArrayList;

public class LegendRecyclerViewAdapter extends RecyclerView.Adapter<LegendRecyclerViewAdapter.ViewHolder> {
    private ArrayList<ChartEntry> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private AdapterView.OnItemClickListener clickListener;
    private Context context;
    public void setOnItemClickListener(AdapterView.OnItemClickListener listener){
        clickListener = listener;
    }

    // data is passed into the constructor
    public LegendRecyclerViewAdapter(Context context,ArrayList<ChartEntry> list) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.legend_recycler_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//    holder.a();
        holder.labelTextView1.setText(mData.get(position).getLabel());
        holder.valueTextView.setText(Integer.toString(mData.get(position).getValue()));
        holder.view.setBackgroundColor(Color.parseColor(mData.get(position).getColor()));

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//        Button button;
        Button button2;
        LinearLayout layout;
        TextView valueTextView;
        TextView labelTextView1;
        View view;
        @SuppressLint("ResourceAsColor")
        ViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            valueTextView = itemView.findViewById(R.id.valueTextView);
            labelTextView1 = itemView.findViewById(R.id.labelTextView);
            view = itemView.findViewById(R.id.view);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

        }

        @SuppressLint("ResourceAsColor")
        public void a(){
            layout = itemView.findViewById(R.id.legendLayout);


        }
    }

    String getItem(int id) {
        return mData.get(id).toString();
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
//    public void addItems(ArrayList<ChartEntry> entries){
//        mData = entries;
//        notifyDataSetChanged();
//    }

    public void createLegend(int color){

    }


}
