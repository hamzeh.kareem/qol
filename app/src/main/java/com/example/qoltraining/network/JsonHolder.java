package com.example.qoltraining.network;

import com.example.qoltraining.user_flow.user_managment.login.model.LoginModel;
import com.example.qoltraining.user_flow.user_managment.login.model.LoginRequestModel;

import io.reactivex.rxjava3.core.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface JsonHolder {
    @POST("api/UserManagement/Login")
    Single<LoginModel> login(@Body LoginRequestModel loginRequestModel);
}
