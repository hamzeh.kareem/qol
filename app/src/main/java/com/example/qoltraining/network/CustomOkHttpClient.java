package com.example.qoltraining.network;

import android.content.Context;
import android.util.Log;

import com.chuckerteam.chucker.api.ChuckerInterceptor;
import com.example.qoltraining.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class CustomOkHttpClient {
    public static OkHttpClient addInterceptor(Context context) {

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client = new OkHttpClient.Builder()
                .addInterceptor(new AuthInterceptor(context))
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new ChuckerInterceptor(context))
                .readTimeout(40, TimeUnit.SECONDS); // connect timeout



        return client.build();
    }


    private static HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(
                        message -> Log.d("API_LOG", message));
        httpLoggingInterceptor.level(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return httpLoggingInterceptor;
    }
}
