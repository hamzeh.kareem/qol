package com.example.qoltraining.data.models;

import com.google.gson.Gson;

import java.util.ArrayList;

public class MatrixChartViewModel {
    private String name, xAxisLabel, yAxisLabel;
    private ArrayList<MatrixStatusModel> xAxisLegend = null;
    private ArrayList<MatrixStatusModel> yAxisLegend = null;
    private ArrayList<ArrayList<MatrixItem>> matrix = null;

    public MatrixChartViewModel() {
    }

    public MatrixChartViewModel(String name, String xAxisLabel, String yAxisLabel, ArrayList<MatrixStatusModel> xAxisLegend, ArrayList<MatrixStatusModel> yAxisLegend, ArrayList<ArrayList<MatrixItem>> matrix) {
        this.name = name;
        this.xAxisLabel = xAxisLabel;
        this.yAxisLabel = yAxisLabel;
        this.xAxisLegend = xAxisLegend;
        this.yAxisLegend = yAxisLegend;
        this.matrix = matrix;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getXAxisLabel() {
        return xAxisLabel == null ? "" : xAxisLabel;
    }

    public void setXAxisLabel(String xAxisLabel) {
        this.xAxisLabel = xAxisLabel;
    }

    public String getYAxisLabel() {
        return yAxisLabel == null ? "" : yAxisLabel;
    }

    public void setYAxisLabel(String yAxisLabel) {
        this.yAxisLabel = yAxisLabel;
    }

    public ArrayList<MatrixStatusModel> getXAxisLegend() {
        return xAxisLegend == null ? new ArrayList<>() : xAxisLegend;
    }

    public void setXAxisLegend(ArrayList<MatrixStatusModel> xAxisLegend) {
        this.xAxisLegend = xAxisLegend;
    }

    public ArrayList<MatrixStatusModel> getYAxisLegend() {
        return yAxisLegend == null ? new ArrayList<>() : yAxisLegend;
    }

    public void setYAxisLegend(ArrayList<MatrixStatusModel> yAxisLegend) {
        this.yAxisLegend = yAxisLegend;
    }

    public ArrayList<ArrayList<MatrixItem>> getMatrix() {
        return matrix == null ? new ArrayList<>() : matrix;
    }

    public void setMatrix(ArrayList<ArrayList<MatrixItem>> matrix) {
        this.matrix = matrix;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
