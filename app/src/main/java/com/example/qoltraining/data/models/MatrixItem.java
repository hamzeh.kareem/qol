package com.example.qoltraining.data.models;

import com.google.gson.Gson;

public class MatrixItem {
    private int count;
    private String itemColor;
    // TODO: 2020-01-18 clean up to be generic better
    private int probabilityStatusCode, impactStatusCode;

    public MatrixItem() {
    }

    public MatrixItem(int count, String itemColor, int probabilityStatusCode, int impactStatusCode) {
        this.count = count;
        this.itemColor = itemColor;
        this.probabilityStatusCode = probabilityStatusCode;
        this.impactStatusCode = impactStatusCode;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getItemColor() {
        return itemColor == null ? "" : itemColor;
    }

    public void setItemColor(String itemColor) {
        this.itemColor = itemColor;
    }

    public int getProbabilityStatusCode() {
        return probabilityStatusCode;
    }

    public void setProbabilityStatusCode(int probabilityStatusCode) {
        this.probabilityStatusCode = probabilityStatusCode;
    }

    public int getImpactStatusCode() {
        return impactStatusCode;
    }

    public void setImpactStatusCode(int impactStatusCode) {
        this.impactStatusCode = impactStatusCode;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
