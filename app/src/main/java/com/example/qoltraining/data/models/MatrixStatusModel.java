package com.example.qoltraining.data.models;

import com.google.gson.Gson;

public class MatrixStatusModel {
    int id;
    private String statusName;
    private String statusColor;

    public MatrixStatusModel() {
    }

    public MatrixStatusModel(int id, String statusName, String statusColor) {
        this.id = id;
        this.statusName = statusName;
        this.statusColor = statusColor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatusName() {
        return statusName == null ? "" : statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusColor() {
        return statusColor == null ? "" : statusColor;
    }

    public void setStatusColor(String statusColor) {
        this.statusColor = statusColor;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
