package com.example.qoltraining.utils;

import androidx.databinding.BindingAdapter;

import com.example.qoltraining.custom_views.MatrixChartView;
import com.example.qoltraining.data.models.MatrixChartViewModel;

public class BindingUtil {
    @BindingAdapter(value = {"matrixChartViewModel"})
    public static void fillMatrixChart(MatrixChartView matrixChartView, MatrixChartViewModel matrixChartViewModel) {
        if (matrixChartViewModel != null)
            matrixChartView.setMatrixChartViewModel(matrixChartViewModel);
    }
}
